require 'course.rb'

class Student

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end


  attr_accessor :first_name, :last_name, :courses


  def name
    "#{first_name} #{last_name}"
  end


  def enroll(new_course)
    @courses.each { |course| raise_error if course.conflicts_with?(new_course) }

    unless @courses.include?(new_course)
      self.courses << new_course
      new_course.students << self
    end
  end


  def course_load
    load_hash = Hash.new(0)
    self.courses.each { |course| load_hash[course.department] += course.credits }
    load_hash
  end

end
